package uk.co.cablepost.conveyorbelts.robotic_arm.iron;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmBlock;

public class IronFilterRoboticArmBlock extends FilterRoboticArmBlock {

    public IronFilterRoboticArmBlock(Settings settings) {
        super(settings, false);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronFilterRoboticArmBlockEntity(pos, state);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                checkType(type, ConveyorBelts.IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, IronFilterRoboticArmBlockEntity::clientTick) :
                checkType(type, ConveyorBelts.IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, IronFilterRoboticArmBlockEntity::serverTick)
        ;
    }
}
