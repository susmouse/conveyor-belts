package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class RoboticArmBlock extends BlockWithEntity {
    public static final BooleanProperty ENABLED;
    public static final DirectionProperty FACING;

    static {
        ENABLED = Properties.ENABLED;
        FACING = Properties.HORIZONTAL_FACING;
    }
    public RoboticArmBlock(Settings settings, boolean skipSetDefaultState) {
        super(settings);

        if(!skipSetDefaultState){
            return;
        }
        this.setDefaultState(
                this.stateManager.getDefaultState()
                        .with(ENABLED, true)
                        .with(FACING, Direction.NORTH)
        );
    }

    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(
                ENABLED,
                FACING
        );
    }

    public BlockState getPlacementState(ItemPlacementContext ctx) {
        Direction facing = ctx.getPlayerFacing().getOpposite();
        PlayerEntity player = ctx.getPlayer();

        if(player == null){
            return this.getDefaultState();
        }

        boolean sneaking = false;
        /*
        if (ctx.getWorld().isClient) {
            sneaking = ConveyorBeltsClient.invertBeltPlacement;
        }
        else{
            Boolean getInv = ConveyorBelts.invertBeltPlacementOfPlayer.get(player.getUuid());
            sneaking = getInv != null && getInv;
        }
        */

        if(!sneaking){
            facing = facing.getOpposite();
        }

        return this.getDefaultState()
                .with(ENABLED, !ctx.getWorld().isReceivingRedstonePower(ctx.getBlockPos()))
                .with(FACING, facing)
        ;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return VoxelShapes.union(
                VoxelShapes.cuboid(0.0f, 0.0f, 0.0f, 1.0f, 0.0625f, 1.0f),
                VoxelShapes.cuboid(0.35f, 0.0625f, 0.35f, 0.65f, 0.3625f, 0.65f)
        );
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof RoboticArmBlockEntity roboticArmBlockEntity) {
                //ItemScatterer.spawn(world, pos, (RoboticArmBlockEntity)blockEntity);

                for(int i = 0; i < roboticArmBlockEntity.size(); ++i) {
                    ItemStack stack = roboticArmBlockEntity.getStack(i);

                    NbtCompound stackNbt = stack.getOrCreateNbt();
                    if(!stackNbt.getBoolean("IsFilterItem")){
                        ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), stack);
                    }
                }

                // update comparators
                world.updateComparators(pos,this);
            }
            super.onStateReplaced(state, world, pos, newState, moved);
        }
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos));
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        //With inheriting from BlockWithEntity this defaults to INVISIBLE, so we need to change that!
        return BlockRenderType.MODEL;
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (!world.isClient) {
            //This will call the createScreenHandlerFactory method from BlockWithEntity, which will return our blockEntity casted to
            //a namedScreenHandlerFactory. If your block class does not extend BlockWithEntity, it needs to implement createScreenHandlerFactory.
            NamedScreenHandlerFactory screenHandlerFactory = state.createScreenHandlerFactory(world, pos);

            if (screenHandlerFactory != null) {
                //With this call the server will request the client to open the appropriate Screenhandler
                player.openHandledScreen(screenHandlerFactory);
            }
        }

        return ActionResult.SUCCESS;
    }

    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean notify) {
        if (!oldState.isOf(state.getBlock())) {
            this.updateEnabled(world, pos, state);
        }
    }

    public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos fromPos, boolean notify) {
        this.updateEnabled(world, pos, state);
    }

    private void updateEnabled(World world, BlockPos pos, BlockState state) {
        boolean bl = !world.isReceivingRedstonePower(pos);
        if (bl != state.get(ENABLED)) {
            world.setBlockState(pos, state.with(ENABLED, bl), 2);
        }
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return null;
    }
}
