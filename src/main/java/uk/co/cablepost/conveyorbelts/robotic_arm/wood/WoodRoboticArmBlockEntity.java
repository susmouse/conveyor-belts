package uk.co.cablepost.conveyorbelts.robotic_arm.wood;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;

public class WoodRoboticArmBlockEntity extends RoboticArmBlockEntity {
    public WoodRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.WOOD_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.WOOD_ROBOTIC_ARM_SPEED;
    }
}
