package uk.co.cablepost.conveyorbelts.robotic_arm.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmBlockEntity;

public class IronFilterRoboticArmBlockEntity extends FilterRoboticArmBlockEntity {
    public IronFilterRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.IRON_ROBOTIC_ARM_SPEED;
    }
}
