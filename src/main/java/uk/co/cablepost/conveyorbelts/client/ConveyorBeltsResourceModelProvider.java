package uk.co.cablepost.conveyorbelts.client;

import net.fabricmc.fabric.api.client.model.ModelProviderContext;
import net.fabricmc.fabric.api.client.model.ModelProviderException;
import net.fabricmc.fabric.api.client.model.ModelResourceProvider;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmModel;

public class ConveyorBeltsResourceModelProvider implements ModelResourceProvider {
    public static final Identifier ROBOTIC_ARM_MODEL = new Identifier("conveyorbelts:block/robotic_arm");
    @Override
    public UnbakedModel loadModelResource(Identifier identifier, ModelProviderContext modelProviderContext) throws ModelProviderException {
        /*
        if(identifier.equals(ROBOTIC_ARM_MODEL)) {
            return new RoboticArmModel();
        } else {
            return null;
        }
        */
        return null;
    }
}